package com.dvereykin.program4;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.*;

/**
 * Created by Dmitry Vereykin aka eXrump on 11/19/2016.
 */

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String TABLE_ADDRESS = "address";
    public static final String COLUMN_ID = "_id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_ADDRESS = "address";
    public static final String COLUMN_CITY = "city";
    public static final String COLUMN_STATE = "state";
    public static final String COLUMN_ZIPCODE = "zipcode";

    private static final String DATABASE_NAME = "PersonalData.db";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_CREATE_SQL = "create table "
            + TABLE_ADDRESS  + "("
            + COLUMN_ID      + " integer primary key autoincrement, "
            + COLUMN_NAME    + " text not null, "
            + COLUMN_ADDRESS + " text not null,"
            + COLUMN_CITY + " text not null,"
            + COLUMN_STATE + " text not null,"
            + COLUMN_ZIPCODE + " text not null);";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(DATABASE_CREATE_SQL);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_ADDRESS);
        onCreate(db);
    }
}

class DataSource {
    private SQLiteDatabase database;
    private DatabaseHelper dbHelper;
    private String[] allColumns = {DatabaseHelper.COLUMN_ID,
            DatabaseHelper.COLUMN_ADDRESS, DatabaseHelper.COLUMN_NAME,
            DatabaseHelper.COLUMN_CITY, DatabaseHelper.COLUMN_STATE,
            DatabaseHelper.COLUMN_ZIPCODE};

    DataSource(Context context) {
        dbHelper = new DatabaseHelper(context);
    }

    public void open() throws SQLException {
        database = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public long createAddress(PDAttributeGroup address) {
        ContentValues values = new ContentValues();

        values.put(DatabaseHelper.COLUMN_NAME, address.name);
        values.put(DatabaseHelper.COLUMN_ADDRESS, address.address);
        values.put(DatabaseHelper.COLUMN_CITY, address.city);
        values.put(DatabaseHelper.COLUMN_STATE, address.state);
        values.put(DatabaseHelper.COLUMN_ZIPCODE, address.zipCode);


        long insertId = database.insert(DatabaseHelper.TABLE_ADDRESS,
                null,
                values);

        return insertId;
    }

    public void deleteAddress(PDAttributeGroup address) {
        long id = address.id;
        database.delete(DatabaseHelper.TABLE_ADDRESS,
                DatabaseHelper.COLUMN_ID + " = " + id, null);
    }

    public PersonalDataCollection getAllAddresses() throws Exception {
        PersonalDataCollection addresses = new PersonalDataCollection();

        Cursor cursor = database.query(DatabaseHelper.TABLE_ADDRESS,
                allColumns, null, null, null, null, null);

        cursor.moveToFirst();

        while (!cursor.isAfterLast()) {
            addresses.addAddress(cursorToAddressAttributeGroup(cursor));
            cursor.moveToNext();
        }

        cursor.close();
        return addresses;
    }

    private PDAttributeGroup cursorToAddressAttributeGroup(Cursor cursor) {
        PDAttributeGroup address = new PDAttributeGroup(cursor.getInt(cursor.getColumnIndex(DatabaseHelper.COLUMN_ID)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_NAME)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ADDRESS)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_CITY)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_STATE)),
                cursor.getString(cursor.getColumnIndex(DatabaseHelper.COLUMN_ZIPCODE)));
        return address;
    }

}
